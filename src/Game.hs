{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DataKinds #-}
module Game where

import           Data.Foldable                  ( for_ )
import           Data.List.NonEmpty             ( fromList )
import           Data.String                    ( IsString(fromString) )
import           Solver                         ( Response
                                                , bestGuess
                                                , possibleAnswers
                                                , respond
                                                , responseToString
                                                )
import           System.Environment             ( getArgs )
import           System.Exit                    ( exitFailure
                                                , exitSuccess
                                                )
import           System.IO                      ( hFlush
                                                , stdout
                                                )
import           Words                          ( WordType(Answer, Guess)
                                                , WordleWord
                                                , finalWords
                                                , guessWords
                                                , randomAnswer
                                                , toString
                                                )


solveWordle :: IO ()
solveWordle = gameLoop [] finalWords
  where
    gameLoop _ [] = do
        putStrLn "There is no possible answer!"
        putStrLn "Perhaps you have mistyped one of your guesses or responses."
        exitFailure
    gameLoop _             [answer] = victory answer
    gameLoop pastResponses answers  = do
        let idealGuess = bestGuess answers $ fromList guessWords
        showBestGuess idealGuess

        nextGuess    <- askGuess
        nextResponse <- askResponse
        let newResponses = pastResponses <> [(nextGuess, nextResponse)]
            remainingAnswers =
                possibleAnswers (nextGuess, nextResponse) answers

        putStrLn ""
        drawPastGuesses newResponses
        gameLoop newResponses remainingAnswers

    drawPastGuesses pastResponses = do
        for_ pastResponses $ \(guess, response) ->
            putStrLn $ toString guess <> " -> " <> responseToString response

    victory :: WordleWord 'Answer -> IO ()
    victory answer = putStrLn $ toString answer <> " -> GGGGG"

    showBestGuess :: WordleWord 'Guess -> IO ()
    showBestGuess guess = do
        putStr "Your next best guess is: "
        hFlush stdout
        putStrLn $ toString guess


    askGuess = fromString <$> readLine "Your guess: "

    askResponse :: IO Response
    askResponse = fromString <$> readLine "Your response: "

playWordle :: IO ()
playWordle = do
    explain
    randomAnswer >>= play
  where
    explain = do
        putStrLn "Welcome to Wordle!"
        putStrLn "Just type in your guess and see the response."
        putStrLn
            "If a letter is in the correct position, it will be marked with a 'G'."
        putStrLn
            "If the letter is somewhere in the word, just not in the right position, it will be marked with a 'Y'."
        putStrLn
            "If the letter is nowhere in the word, it will be marked with an 'X'."

    play answer = do
        guess <- completeLine
            (\word ->
                " -> " <> responseToString (respond answer $ fromString word)
            )
        if fromString guess == answer then exitSuccess else play answer

readLine :: String -> IO String
readLine query = do
    putStr query
    hFlush stdout
    getLine

completeLine :: (String -> String) -> IO String
completeLine getRest = do
    input <- getLine
    moveCursorUp
    putStrLn $ input <> getRest input
    pure input

moveCursorUp :: IO ()
moveCursorUp = putStr "\o33[1A"

gameMain :: IO ()
gameMain = getArgs >>= \case
    ["play" ] -> playWordle
    ["solve"] -> solveWordle
    _         -> do
        putStrLn "Possible commands:"
        putStrLn "\tplay\tPlay a game of wordle"
        putStrLn "\tsolve\t Solve a game of wordle"

