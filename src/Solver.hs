{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DataKinds #-}
module Solver where

import           Prelude                 hiding ( words )

import           Control.DeepSeq                ( NFData )
import qualified Data.ByteString.Char8         as BS
import           Data.Foldable                  ( Foldable(toList) )
import           Data.Function                  ( (&) )
import           Data.List                      ( genericLength )
import           Data.List.NonEmpty             ( NonEmpty )
import           Data.Maybe                     ( fromMaybe )
import           Data.String                    ( IsString(fromString) )
import           GHC.Generics                   ( Generic )
import           Util                           ( averageOnDef
                                                , count
                                                , minimumOn1
                                                )
import           Words                          ( WordType(Answer, Guess)
                                                , WordleWord(WordleWord)
                                                )

bestGuess
    :: [WordleWord 'Answer]
    -> NonEmpty (WordleWord 'Guess)
    -> WordleWord 'Guess
bestGuess = minimumOn1 . averageRemainingAnswers
{-# SCC bestGuess #-}

averageRemainingAnswers
    :: [WordleWord 'Answer] -> WordleWord 'Guess -> Rational
averageRemainingAnswers answers guess =
    answers
        & fmap (`learn` guess)
        & averageOnDef 0 (`countPossibleAnswers` answers)
        & toRational
{-# SCC averageRemainingAnswers #-}

countPossibleAnswers
    :: (WordleWord 'Guess, Response) -> [WordleWord 'Answer] -> Integer
countPossibleAnswers guessResp = genericLength . possibleAnswers guessResp
{-# SCC countPossibleAnswers #-}

possibleAnswers
    :: (WordleWord 'Guess, Response)
    -> [WordleWord 'Answer]
    -> [WordleWord 'Answer]
possibleAnswers = filter . isPossibleAnswer
{-# SCC possibleAnswers #-}

learn
    :: WordleWord 'Answer
    -> WordleWord 'Guess
    -> (WordleWord 'Guess, Response)
learn answer guess = (guess, respond answer guess)
{-# SCC learn #-}

isPossibleAnswer
    :: (WordleWord 'Guess, Response) -> WordleWord 'Answer -> Bool
isPossibleAnswer (guess, response) answer = respond answer guess == response
{-# SCC isPossibleAnswer #-}

data LetterInformation
  = Grey
  | Yellow
  | Green
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (NFData)

newtype Response = Response (Vec5 LetterInformation)
  deriving (Eq, Ord)
  deriving newtype (NFData)

instance Show Response where
    show response = show (responseToString response)

instance IsString Response where
    fromString = fromMaybe (error "Invalid response") . parseResponse

respond :: WordleWord 'Answer -> WordleWord 'Guess -> Response
respond (WordleWord answer) (WordleWord guess) = Response
    $ generateVec5 respondLetter
  where
    respondLetter i =
        let
            answerChar           = {-# SCC "answerChar" #-} answer `BS.index` i
            guessChar            = {-# SCC "guessChar" #-} guess `BS.index` i
            guessCharCountTotal  = {-# SCC "guessCharCountTotal" #-} BS.count guessChar answer
            guessCharCountBefore = {-# SCC "guessCharCountBefore" #-} BS.count guessChar $ BS.take i guess
            guessCharGreenSpots  = {-# SCC "guessCharGreenSpots" #-} count
                (\j ->
                    (guessChar == guess `BS.index` j)
                        && (guessChar == answer `BS.index` j)
                )
                [0 .. 4]
        in
             if
                | answerChar == guessChar
                -> Green
                | -- The first check is reduntant, but serves as a
                  -- cheap breakout condition. Improves performance.
                  guessCharCountTotal >  0
                    && guessCharCountTotal > guessCharGreenSpots
                    && guessCharCountTotal > guessCharCountBefore
                -> Yellow
                | otherwise
                -> Grey
    {-# SCC respondLetter #-}
{-# SCC respond #-}

parseResponse :: String -> Maybe Response
parseResponse input =
    Response <$> (traverse charToLetterInfo =<< listToVec5 input)
  where
    charToLetterInfo 'G' = Just Green
    charToLetterInfo 'Y' = Just Yellow
    charToLetterInfo 'X' = Just Grey
    charToLetterInfo _   = Nothing
{-# SCC parseResponse #-}

responseToString :: Response -> String
responseToString (Response response) = toChar <$> toList response
  where
    toChar Grey   = 'X'
    toChar Yellow = 'Y'
    toChar Green  = 'G'
{-# SCC responseToString #-}

 -- We always inspect the first element, so we can make it strict.
data Vec5 a = Vec5 !a a a a a
    deriving stock (Eq, Ord, Functor, Foldable, Traversable, Generic)
    deriving anyclass NFData

generateVec5 :: (Int -> a) -> Vec5 a
generateVec5 f = f <$> Vec5 0 1 2 3 4
{-# SCC generateVec5 #-}

listToVec5 :: [a] -> Maybe (Vec5 a)
listToVec5 [a0, a1, a2, a3, a4] = Just $ Vec5 a0 a1 a2 a3 a4
listToVec5 _                    = Nothing
{-# SCC listToVec5 #-}
