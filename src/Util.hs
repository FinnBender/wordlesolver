module Util where

import           Control.Arrow                  ( Arrow(first) )
import           Control.Parallel.Strategies    ( NFData
                                                , parMap
                                                , rdeepseq
                                                )
import           Data.Foldable                  ( Foldable(foldl') )
import           Data.Function                  ( (&) )
import           Data.List.NonEmpty             ( nonEmpty )
import qualified Data.MultiSet                 as MS
import           Data.Ratio                     ( (%)
                                                , Ratio
                                                )
import           Data.Semigroup                 ( Arg(Arg)
                                                , Min(Min, getMin)
                                                )
import           Numeric.Natural                ( Natural )


minimumOn1 :: (Foldable1 f, Ord b) => (a -> b) -> f a -> a
minimumOn1 f = getArg . getMin . foldMap1 (\x -> Min $ Arg (f x) x)
    where getArg (Arg _ arg) = arg

averageOnDef
    :: (Ord a, Integral b, NFData b) => Ratio b -> (a -> b) -> [a] -> Ratio b
averageOnDef def f xs =
    let occList = MS.fromList xs & MS.toOccurList & parMap rdeepseq (first f)
    in  case nonEmpty occList of
            Nothing -> def
            Just ys -> getAvg $ foldMap1
                (\(y, n) -> Avg (fromIntegral n * y) (fromIntegral n))
                ys


-- | Counts the number of elements matching the predicate
--
-- prop> count f = length . filter f
count :: Foldable f => (a -> Bool) -> f a -> Int
count f = foldl' go 0
  where
    go n a | f a       = n + 1
           | otherwise = n

-- | Repeat an action until the returned value is @Just someValue@. Then return @someValue@.
untilJustM :: Monad m => m (Maybe a) -> m a
untilJustM mma = go where go = mma >>= maybe go pure

data Avg a = Avg !a !Natural

getAvg :: Integral a => Avg a -> Ratio a
getAvg (Avg x n) = x % fromIntegral n

instance Num a => Semigroup (Avg a) where
    Avg x n <> Avg y m = Avg (x + y) (m + n)
