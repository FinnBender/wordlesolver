{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Main where

import           Data.Function                  ( (&) )
import           Solver                         ( averageRemainingAnswers
                                                , countPossibleAnswers
                                                , isPossibleAnswer
                                                , learn
                                                , possibleAnswers
                                                , respond
                                                )
import           Test.Hspec.Expectations        ( shouldBe )
import           Test.Tasty                     ( TestTree
                                                , localOption
                                                , testGroup
                                                )
import           Test.Tasty.Bench               ( Benchmark
                                                , bench
                                                , bgroup
                                                , defaultMain
                                                , nf
                                                )
import           Test.Tasty.HUnit               ( testCase )
import           Test.Tasty.QuickCheck          ( (==>)
                                                , Arbitrary(arbitrary, shrink)
                                                , QuickCheckTests
                                                    ( QuickCheckTests
                                                    )
                                                , elements
                                                , testProperty
                                                )
import           Words                          ( WordType(Answer, Guess)
                                                , WordleWord
                                                , finalWords
                                                , guessWords
                                                , toAnswer
                                                , toString
                                                )


main :: IO ()
main = defaultMain [tests, benchmarks]

tests :: TestTree
tests = testGroup "Tests" [knowledgeTests, solverTests]

knowledgeTests :: TestTree
knowledgeTests = testGroup "Responses" [responseTests]

responseTests :: TestTree
responseTests = testGroup
    "respond"
    [ -- shall
      respondTest "shall" "saner" "GYXXX"
    , respondTest "shall" "cloth" "XYXXY"
    , respondTest "shall" "aback" "XXGXX"
    , respondTest "shall" "adult" "YXXGX"
    , respondTest "shall" "shale" "GGGGX"
      -- fewer
    , respondTest "fewer" "raise" "YXXXY"
    , respondTest "fewer" "outer" "XXXGG"
    , respondTest "fewer" "fence" "GGXXY"
    , respondTest "fewer" "fever" "GGXGG"
      -- synthetic tests
    , respondTest "aaaaa" "aabaa" "GGXGG"
    , respondTest "babab" "aaaaa" "XGXGX"
    , respondTest "cbcbc" "babab" "YXYXX"
    , respondTest "abcde" "bcdea" "YYYYY"
    , respondTest "xxxxa" "aaaay" "YXXXX"
    , respondTest "yyaay" "xaaxx" "XYGXX"
    ]
  where
    respondTest answer guess response = testCase
        (unwords [toString answer, toString guess])
        (respond answer guess `shouldBe` response)

solverTests :: TestTree
solverTests =
    testGroup
            "Wordle"
            [ testProperty "wordle property" $ \answer guess ->
                answer `elem` possibleAnswers (learn answer guess) finalWords
            , testProperty "inverse wordle property" $ \answer guess ->
                answer
                    /=        toAnswer guess
                    ==>       toAnswer guess
                    `notElem` possibleAnswers (learn answer guess) finalWords
            ]
        & localOption (QuickCheckTests 1000)



benchmarks :: Benchmark
benchmarks = bgroup "Benchmarks" [solverBenchmarks]

solverBenchmarks :: Benchmark
solverBenchmarks = bgroup
    "Solver"
    [ bench "respond (green)" $ nf (respond "aaaaa") "aaaaa"
    , bench "respond (yellow)" $ nf (respond "abcde") "bcdea"
    , bench "respond (grey)" $ nf (respond "aaaaa") "bbbbb"
    , bench "isPossibleAnswer"
        $ nf (isPossibleAnswer (learn "thorn" "light")) "thick"
    , bench "possibleAnswers"
        $ nf (possibleAnswers (learn "house" "flour")) finalWords
    , bench "countPossibleAnswers"
        $ nf (countPossibleAnswers (learn "house" "flour")) finalWords
    , bench "averageRemainingAnswers"
        $ nf (averageRemainingAnswers finalWords) "night"
    ]

instance Arbitrary (WordleWord 'Answer) where
    arbitrary = elements finalWords
    shrink _ = []

instance Arbitrary (WordleWord 'Guess) where
    arbitrary = elements guessWords
    shrink _ = []
