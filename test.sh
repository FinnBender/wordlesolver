#! /usr/bin/env sh

cd "${BASH_SOURCE%/*}" || exit 1

BENCH_FILE="test-baseline.csv"

generate_new_csv=false

if [ ! -f "$BENCH_FILE" ] || [ "$1" = "-b" ]; then
    generate_new_csv=true
fi

test_args=""

if [ "$generate_new_csv" = true ]; then
    test_args="$test_args --csv \"$BENCH_FILE\""
else
    test_args="$test_args --baseline \"$BENCH_FILE\""
fi

cabal test --test-show-details=streaming --test-options="$test_args"
